package com.zuitt.discussion.config;

import com.zuitt.discussion.services.JwtUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

// This annonation enables web securities in our application defined by WebSecurityConfig implementations.
@Configuration
// This annonation enables the web securities defined by WebSecurityConfigurerAdapter automatically.
@EnableWebSecurity
// This annotation is used in Spring Security to enable global method-level security for protecting methods in your application.
// This is used to apply security measures at the method level, rather than just at the URL level.
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private JwtAuthenticate jwtAuthenticate;

    // injects the "JwtUserDetailsService" class which handles the user details for JWT authentication
    @Autowired
    private JwtUserDetailsService jwtUserDetailsService;

    // injects the "JwtRequestFilter" class which has a custom filter for handling JWT requests.
    // Filters to be applied on the request
    // Jwt authentication
    // username authentication
    @Autowired
    private JwtRequestFilter jwtRequestFilter;

    @Autowired
    // This method configures the "AuthenticationManager" by specifying the "jwtUserDetailsService".
    // This allows the authentication manager to authenticate users based on the provided user details service and password encoder.
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {

        auth.userDetailsService(jwtUserDetailsService).passwordEncoder(passwordEncoder());

    }


    // @Bean annotation is used in Spring Boot to make a special method that creates a manages an object that can be used in different parts of our application.

    // What to do during authentication
    // This serves as the authentication entry point for handling unauthorized requests.
    @Bean
    public JwtAuthenticate jwtAuthenticationEntryPointBean() throws Exception{
        return new JwtAuthenticate();
    }

    // Instantiates a BCryptPasswordEncoder object for password hashing/encoding.
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    // The method is used in the configuration of Spring Security to expose the "AuthenticationManager" as a Bean in the Spring context. By doing this, it can be injected into other parts of the application for authentication-related tasks.
    // Example: it can be used to authenticate user credentials during the login process.
    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();

    }

    // Routes that will not require Jwt Tokens
    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        // This enables the CORS and disabling CSRF protection in the Spring Security of the application. The CSRF is disable because were using JWT on verifying the web requests.
        // CORS: allows web browsers to make cross-origin HTTP requests from one domain to another domain, which is typically restricted by the browser's same-origin policy.
        // CSRF: is an attack that tricks a user into performing an action on a different website without their consent.
        httpSecurity.cors().and().csrf().disable()
                // This starts of configuring the authorization rules, which means the following line of code are specifying which URLs should be accessible or restricted based on authentication and authorization rules
                .authorizeRequests()
                // allows the following endpoint to be accessible without requiring any authentication.
                .antMatchers("/authenticate").permitAll()
                .antMatchers("/users/register").permitAll()
                // allows the "/posts" endpoint to be accessible via GET method without requiring any authentication
                .antMatchers(HttpMethod.GET, "/posts").permitAll()
                // allows unrestricted access to HTTP OPTIONS requests made to any URL in the application, regardless of the path, without requiring any authentication.
                .antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
                // This specifies that any other request not matching the above rules should be authenticated, meaning that it requires proper authentication to be accessed.
                .anyRequest().authenticated().and()
                // This configures the authentication entry point for handling authentication failures.
                .exceptionHandling().authenticationEntryPoint(jwtAuthenticate).and()
                // This is used to specify that the application should not create or use any sessions, as it is following a stateless authentication approach using JSON Web Tokens (JWT).
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        // This is used to add a custom filter (JwtRequestFilter) before a specific filter class (UsernamePasswordAuthenticationFilter)
        // "jwtRequestFilter" is responsible for handling JWT authentication and authorization logic.
        // "UsernamePasswordAuthenticationFilter" is a built-in filter in Spring Security that handles form-based authentication using username and password.
        httpSecurity.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);
    }
}
