// Classes
package com.zuitt.discussion.controllers;

import com.zuitt.discussion.config.JwtToken;
import com.zuitt.discussion.models.Post;
import com.zuitt.discussion.models.User;
import com.zuitt.discussion.repositories.PostRepository;
import com.zuitt.discussion.repositories.UserRepository;
import com.zuitt.discussion.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
// Enables cross origin request via @CrossOrigin.
@CrossOrigin
public class PostController {
    @Autowired
    PostService postService;

    // Create Post
    @RequestMapping(value="/posts", method= RequestMethod.POST)
    public ResponseEntity<Object> createPost(@RequestHeader(value = "Authorization") String stringToken, @RequestBody Post post){
        postService.createPost(stringToken, post);
        return new ResponseEntity<>("Post created successfully.", HttpStatus.CREATED);
    }

    // Get All Posts
    @RequestMapping(value="/posts", method=RequestMethod.GET)
    public ResponseEntity<Object> getPost(){
        return new ResponseEntity<>(postService.getPost(), HttpStatus.OK);
    }

    // Delete a post
    @RequestMapping(value="/posts/{postid}", method=RequestMethod.DELETE)
    public ResponseEntity<Object> deletePost(@PathVariable Long postid){
        return postService.deletePost(postid);
    }

    // Update a post
    @RequestMapping(value="/posts/{postid}", method=RequestMethod.PUT)
    public ResponseEntity<Object> updatePost(@PathVariable Long postid, @RequestBody Post post){
        return postService.updatePost(postid, post);
    }
}
